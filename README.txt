===THIS REPOSITORY HAS MOVED TO STASH AND IS NOW READ ONLY. SEE https://stash.atlassian.com/projects/SD/repos/confluence-knowledge-base===

A Blueprint for creating Knowledge Base spaces.

This blueprint interacts with the JIRA Service Desk to make KB articles available to ServiceDesk users, and to allow
KB readers to create ServiceDesk requests.

See https://pug.jira.com/wiki/display/BP/Blueprints+to+Summit for an overview of the KB project.