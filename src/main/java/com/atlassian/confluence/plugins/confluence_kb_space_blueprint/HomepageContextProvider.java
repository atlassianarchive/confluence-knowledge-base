package com.atlassian.confluence.plugins.confluence_kb_space_blueprint;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.*;
import com.atlassian.confluence.renderer.template.*;

import java.util.*;

import static com.google.common.collect.Maps.*;

public class HomepageContextProvider extends AbstractBlueprintContextProvider
{
    public static final String LIVESEARCH_MACRO_RESOURCE = "com.atlassian.confluence.plugins.confluence-knowledge-base:space-kb-web-resource";
    public static final String LIVESEARCH_MACRO_TEMPLATE = "Confluence.SpaceBlueprints.KnowledgeBase.livesearchMacro.soy";

    private final TemplateRenderer templateRenderer;

    public HomepageContextProvider(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected BlueprintContext updateBlueprintContext(BlueprintContext context)
    {
        context.put("livesearchMacro", getLivesearchMacro(context));
        return context;
    }

    private String getLivesearchMacro(BlueprintContext context)
    {
        HashMap<String, Object> soyContext = newHashMap();
        soyContext.put("spaceKey", context.getSpaceKey());

        StringBuilder output = new StringBuilder();
        templateRenderer.renderTo(output, LIVESEARCH_MACRO_RESOURCE, LIVESEARCH_MACRO_TEMPLATE, soyContext);
        return output.toString();

    }
}
