package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.rest;

import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.events.KbSearchPerformedEvent;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.SearchResultAugmenter;
import com.atlassian.confluence.plugins.search.api.Searcher;
import com.atlassian.confluence.plugins.search.api.model.SearchQueryParameters;
import com.atlassian.confluence.plugins.search.api.model.SearchResult;
import com.atlassian.confluence.plugins.search.api.model.SearchResultList;
import com.atlassian.confluence.plugins.search.api.model.SearchResults;
import com.atlassian.confluence.search.service.DateRangeEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Provide custom search functionality for ServiceDesk. This class is a knock-off of SearchResource in
 * the confluence-search plugin.
 */
@Path("/search")
@Produces({APPLICATION_JSON + ";charset=UTF-8"})
public class SearchResource
{
    private final TransactionTemplate transactionTemplate;
    private final Searcher searcher;
    private final EventPublisher eventPublisher;
    private final PluginAccessor pluginAccessor;
    private final SearchResultAugmenter searchResultAugmenter;

    public SearchResource(TransactionTemplate transactionTemplate, Searcher searcher, EventPublisher eventPublisher,
        PluginAccessor pluginAccessor, SearchResultAugmenter searchResultAugmenter)
    {
        this.transactionTemplate = transactionTemplate;
        this.searcher = searcher;
        this.eventPublisher = eventPublisher;
        this.pluginAccessor = pluginAccessor;
        this.searchResultAugmenter = searchResultAugmenter;
    }

    @GET
    @AnonymousAllowed
    public Response search(
        final @QueryParam("user") String user,
        final @QueryParam("queryString") String query,
        final @QueryParam("startIndex") @DefaultValue ("0") int startIndex,
        final @QueryParam("pageSize") @DefaultValue("10") int pageSize,
        final @QueryParam("type") String type,
        final @QueryParam("where") String where,
        final @QueryParam("lastModified") String lastModified,
        final @QueryParam("contributor") String contributor,
        final @QueryParam("contributorUsername") String contributorUsername,
        final @QueryParam("includeArchivedSpaces") boolean includeArchivedSpaces,
        final @QueryParam("sessionUuid") String sessionUuid,
        final @QueryParam("labels") Set<String> labels,
        final @QueryParam("highlight") @DefaultValue("true") boolean highlight)
    {
        // abort if the user has been logged out
        if (StringUtils.isNotBlank(user) && !user.equals(AuthenticatedUserThreadLocal.getUsername()))
        {
            return Response.status(401).build();
        }

        return transactionTemplate.execute(new TransactionCallback<Response>()
        {
            @Override
            public Response doInTransaction()
            {
                SearchQueryParameters.Builder builder = new SearchQueryParameters.Builder(query)
                    .startIndex(startIndex)
                    .pageSize(pageSize)
                    .pluggableContentType(pluginAccessor, type)
                    .where(where)
                    .contributor(Strings.isNullOrEmpty(contributorUsername) ? contributor : contributorUsername)
                    .includeArchivedSpaces(includeArchivedSpaces)
                    .highlight(highlight)
                    .labels(labels);

                if (!Strings.isNullOrEmpty(lastModified))
                {
                    DateRangeEnum lastModifiedDateRange = null;
                    try
                    {
                        lastModifiedDateRange = DateRangeEnum.valueOf(lastModified);
                    }
                    catch (IllegalArgumentException e)
                    {
                        // if illegal lastModified passed just ignore
                    }

                    builder.lastModified(lastModifiedDateRange);
                }

                final SearchQueryParameters searchQueryParameters = builder.build();
                SearchResults searchResults = searcher.search(searchQueryParameters, false);
                List<SearchResult> searchResultEntities = searchResults.getResults();

                List<SearchResult> resultsWithLikes = searchResultAugmenter.addLikeCountsToResults(searchResultEntities);

                SearchResultList searchResultList = new SearchResultList(resultsWithLikes,
                    searchResults.getTotalSize(),
                    searchResults.getExtendedTotalSize(),
                    searchResults.getUuid().toString(),
                    searchResults.getTimeSpent());

                publishKbSearchPerformedEvent(searchQueryParameters, searchResults, sessionUuid);

                return Response.ok(searchResultList).build();
            }
        });
    }

    private void publishKbSearchPerformedEvent(final SearchQueryParameters searchQuery, final SearchResults searchResults,
                                               final String sessionUuid)
    {
        String incomingUuid = sessionUuid;
        if (StringUtils.isBlank(incomingUuid))
        {
            incomingUuid = UUID.randomUUID().toString();
        }
        SearchQuery searchv2Query = searchQuery.toSearchV2Query(ImmutableMap.of("sessionUuid", incomingUuid));
        eventPublisher.publish(new KbSearchPerformedEvent(this, searchv2Query, AuthenticatedUserThreadLocal.get(),
                searchResults.getTotalSize()));
    }
}
