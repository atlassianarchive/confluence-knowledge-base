package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services;

import com.atlassian.confluence.plugins.search.api.model.SearchResult;

import java.util.List;

/**
 * Augments Search V3 {@link SearchResult}s with things we need.
 */
public interface SearchResultAugmenter
{
    /**
     * Adds the number of Likes to the metadata for each result.
     *
     * @return a new List with new search results
     */
    List<SearchResult> addLikeCountsToResults(List<SearchResult> results);
}
