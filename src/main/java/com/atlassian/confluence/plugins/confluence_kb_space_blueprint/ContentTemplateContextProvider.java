package com.atlassian.confluence.plugins.confluence_kb_space_blueprint;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.*;
import com.atlassian.confluence.renderer.template.*;
import com.atlassian.plugin.*;
import org.apache.commons.lang3.*;

import java.util.*;

import static com.google.common.collect.Maps.newHashMap;

/**
 * This provider adds dynamic data for substitution into the content-template's \<at:var> elements.
 */
public class ContentTemplateContextProvider extends AbstractBlueprintContextProvider
{
    static final String LABELS_PARAMETER = "labelsString";
    static final String CONTENTBYLABEL_MACRO_RESOURCE = "com.atlassian.confluence.plugins.confluence-knowledge-base:kb-article-resources";
    static final String CONTENTBYLABEL_MACRO_TEMPLATE = "Confluence.Blueprints.Plugin.KnowledgeBaseArticle.contentbylabelMacro.soy";

    private String defaultBlueprintLabel;

    private final TemplateRenderer templateRenderer;

    public ContentTemplateContextProvider(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
        defaultBlueprintLabel = params.get("blueprintLabel");
    }

    @Override
    protected BlueprintContext updateBlueprintContext(BlueprintContext context)
    {

        String labels = (String) context.get(LABELS_PARAMETER);
        if (StringUtils.isBlank(labels)) // default to template label if none specified by user
        {
            labels = defaultBlueprintLabel;
        }

        context.put("contentbylabelMacro", getContentbylabelMacro(context.getSpaceKey(), labels));
        return context;
    }

    private String getContentbylabelMacro(String spaceKey, String labels)
    {
        HashMap<String, Object> soyContext = newHashMap();
        soyContext.put("spaceKey", spaceKey);
        soyContext.put("labels", labels);

        StringBuilder output = new StringBuilder();
        templateRenderer.renderTo(output, CONTENTBYLABEL_MACRO_RESOURCE, CONTENTBYLABEL_MACRO_TEMPLATE, soyContext);
        return output.toString();

    }
}
