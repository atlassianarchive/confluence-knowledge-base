package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.impl;

import com.atlassian.bonnie.Searchable;

import java.util.Collection;

/**
 * A dummy Searchable with an id, because the LikeManager countLikes method requires Searchables and not ids.
 */
class SearchableWithId implements Searchable
{
    private final long id;

    SearchableWithId(long id)
    {
        this.id = id;
    }

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Collection getSearchableDependants()
    {
        return null;
    }

    @Override
    public boolean isIndexable()
    {
        return false;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchableWithId that = (SearchableWithId) o;

        return id == that.id;
    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }
}
