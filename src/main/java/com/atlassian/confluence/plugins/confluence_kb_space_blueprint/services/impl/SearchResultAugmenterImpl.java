package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.impl;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.SearchResultAugmenter;
import com.atlassian.confluence.plugins.search.api.model.SearchResult;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class SearchResultAugmenterImpl implements SearchResultAugmenter
{
    public static final String LIKES_METADATA_KEY = "likes";
    public static final List<String> LIKABLE_TYPES = Lists.newArrayList(Page.CONTENT_TYPE, BlogPost.CONTENT_TYPE, Comment.CONTENT_TYPE);

    private final LikeManager likeManager;

    public SearchResultAugmenterImpl(LikeManager likeManager)
    {
        this.likeManager = likeManager;
    }

    @Override
    public List<SearchResult> addLikeCountsToResults(List<SearchResult> results)
    {
        List<SearchResult> resultsWithLikes = Lists.newArrayList();

        Map<SearchResult, Searchable> searchablesMap = getSearchablesMap(results);
        // We wrap the values() ref in a list for equality testing
        List<Searchable> searchables = Lists.newArrayList(searchablesMap.values());
        if (searchables.isEmpty())
            return results;     // no results are likeable so there's nothing for the likeManager to do.

        Map<Searchable,Integer> likeMap = likeManager.countLikes(searchables);
        for (SearchResult resultEntity : results)
        {
            // Results for content-types that cannot be liked don't get updated.
            SearchResult finalResult = resultEntity;
            if (searchablesMap.containsKey(resultEntity))
            {
                int likes = likeMap.get(searchablesMap.get(resultEntity));
                finalResult = makeResultWithLike(resultEntity, likes);
            }
            resultsWithLikes.add(finalResult);
        }

        return resultsWithLikes;
    }

    // SearchResults are immutable, so to add to the metadata we need to make a copy and "modify" it.
    private SearchResult makeResultWithLike(SearchResult result, int likes)
    {
        Map<String, String> metadata = Maps.newHashMap(result.getMetadata());
        metadata.put(LIKES_METADATA_KEY, Integer.toString(likes));

        return new SearchResult(result.getId(), result.getContentType(), result.getTitle(),
                result.getBodyTextHighlights(), result.getUrl(), result.getSearchResultContainer(),
                result.getFriendlyDate(), result.getExplanation(), ImmutableMap.copyOf(metadata));
    }

    private Map<SearchResult,Searchable> getSearchablesMap(List<SearchResult> results)
    {
        Map<SearchResult,Searchable> map = Maps.newHashMap();
        for (SearchResult result : results)
        {
            if (!LIKABLE_TYPES.contains(result.getContentType()))
                continue;

            long id = result.getId();
            map.put(result, new SearchableWithId(id));
        }
        return map;
    }
}
