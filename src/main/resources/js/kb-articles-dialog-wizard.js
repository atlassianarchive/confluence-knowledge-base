AJS.toInit(function($){

    function postRender(e, state) {
        var wizardForm = state.$container;
        var labelsField = $("#kb-article-labels", wizardForm);
        labelsField.auiSelect2(Confluence.UI.Components.LabelPicker.build({
            separator : " ",
            queryOpts : {
                spaceKey : state.wizardData.spaceKey
            }
        }));
    }

    function validate($container, spaceKey) {
        var wizardForm = $container,
            $titleField = wizardForm.find('#kb-article-title'),
            pageTitle = $.trim($titleField.val()),
            error;

        wizardForm.find('.error').html(''); // clear all existing errors

        if (!pageTitle) {
            error = AJS.I18n.getText("com.atlassian.confluence.plugins.confluence-knowledge-base.kb-how-to-article.wizard.form.name.required");
        }
        else if (!Confluence.Blueprint.canCreatePage(spaceKey, pageTitle)) {
            error = AJS.I18n.getText("com.atlassian.confluence.plugins.confluence-knowledge-base.kb-how-to-article.wizard.form.name.exists");
        }
        if (error) {
            $titleField.focus().siblings(".error").html(error);
            return false;
        }

        return true;
    }

    function submit(e, state) {
        return validate(state.$container, state.wizardData.spaceKey);
    }

    // Register wizard hooks
    Confluence.Blueprint.setWizard('com.atlassian.confluence.plugins.confluence-knowledge-base:kb-how-to-item', function(wizard) {
        wizard.on('post-render.kb-how-to-wizard', postRender);
        wizard.on('submit.kb-how-to-wizard', submit);
    });

    // Register wizard hooks
    Confluence.Blueprint.setWizard('com.atlassian.confluence.plugins.confluence-knowledge-base:kb-troubleshooting-item', function(wizard) {
        wizard.on('post-render.kb-troubleshooting-wizard', postRender);
        wizard.on('submit.kb-troubleshooting-wizard', submit);
    });

});
