AJS.bind("blueprint.wizard-register.ready", function () {
    function submitKbSpace(e, state) {
        state.pageData.ContentPageTitle = state.pageData.name;
        return Confluence.SpaceBlueprint.CommonWizardBindings.submit(e, state);
    }

    function preRenderKbSpace(e, state) {
        state.soyRenderContext['atlToken'] = AJS.Meta.get('atl-token');
        state.soyRenderContext['showSpacePermission'] = false;
    }

    Confluence.Blueprint.setWizard('com.atlassian.confluence.plugins.confluence-knowledge-base:kb-blueprint-item', function(wizard) {
        wizard.on("submit.kbSpaceId", submitKbSpace);
        wizard.on("pre-render.kbSpaceId", preRenderKbSpace);
        wizard.on("post-render.kbSpaceId", Confluence.SpaceBlueprint.CommonWizardBindings.postRender);
    });
});