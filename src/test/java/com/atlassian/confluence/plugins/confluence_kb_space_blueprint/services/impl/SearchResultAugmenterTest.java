package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.impl;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.services.SearchResultAugmenter;
import com.atlassian.confluence.plugins.search.api.model.SearchResult;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchResultAugmenterTest
{
    @Mock private LikeManager likeManager;

    private SearchResultAugmenter augmenter;

    @Before
    public void setUp() throws Exception
    {
        augmenter = new SearchResultAugmenterImpl(likeManager);
    }

    @Test
    public void testAddLikeCountsWithNoResults() throws Exception
    {
        List<SearchResult> results = Lists.newArrayList();

        List<SearchResult> resultsWithLikes = augmenter.addLikeCountsToResults(results);

        assertThat(resultsWithLikes.size(), is(results.size()));
    }

    @Test
    public void testAddLikeCounts() throws Exception
    {
        SearchResult pageResult = makeSearchResult(1L, Page.CONTENT_TYPE);
        SearchResult spaceDescResult = makeSearchResult(2L, SpaceDescription.CONTENT_TYPE_SPACEDESC);

        List<SearchResult> originalResults = Lists.newArrayList(pageResult, spaceDescResult);

        SearchableWithId pageSearchable = new SearchableWithId(pageResult.getId());
        Collection<Searchable> expectedSearchables = Lists.<Searchable>newArrayList(pageSearchable);

        Map<Searchable, Integer> likesMap = Maps.newHashMap();
        likesMap.put(pageSearchable, 5);  // five likes for the page

        when(likeManager.countLikes(expectedSearchables)).thenReturn(likesMap);

        List<SearchResult> finalResults = augmenter.addLikeCountsToResults(originalResults);

        assertThat(finalResults.size(), is(originalResults.size()));

        assertThat(finalResults.get(0).getMetadata().get("likes"), is("5"));
        assertNull(finalResults.get(1).getMetadata().get("likes"));
    }

    private SearchResult makeSearchResult(long id, String contentType)
    {
        return new SearchResult(id, contentType, null, null, null, null, null, null, Maps.<String,String>newHashMap());
    }
}
