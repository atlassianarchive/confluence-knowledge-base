package com.atlassian.confluence.plugins.confluence_kb_space_blueprint;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.maven.MavenDependency;
import com.atlassian.confluence.it.maven.MavenDependencyHelper;
import com.atlassian.confluence.it.maven.MavenUploadablePlugin;
import com.atlassian.confluence.it.plugin.UploadablePlugin;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;

import java.io.IOException;

/**
 * Assists with installing the Knowledge Base plugin.
 */
public class KbPluginHelper
{
    private static final String GROUP_ID = "com.atlassian.confluence.plugins";
    private static final String ARTIFACT_ID = "confluence-knowledge-base";

    // HACK - a bit crap - this needs updating to match the version in the pom :/
    private static final String VERSION = "1.0-SNAPSHOT";
    private static final String NAME = "Confluence Knowledge Base Blueprints Plugin";

    private final ConfluenceRpc rpc;

    public KbPluginHelper(ConfluenceRpc rpc)
    {
        this.rpc = rpc;
    }

    public void installPluginIfNotInstalled() throws IOException
    {
        rpc.logIn(User.ADMIN);
        rpc.getPluginHelper().installPluginIfNotInstalled(getKBPlugin());
    }

    private UploadablePlugin getKBPlugin()
    {
        MavenDependency pluginDependency = MavenDependencyHelper.resolve(GROUP_ID,
            ARTIFACT_ID, VERSION);
        return new MavenUploadablePlugin(GROUP_ID + "." + ARTIFACT_ID, NAME, pluginDependency);
    }
}
