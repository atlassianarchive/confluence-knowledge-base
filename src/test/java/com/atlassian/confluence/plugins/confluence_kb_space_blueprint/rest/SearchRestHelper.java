package com.atlassian.confluence.plugins.confluence_kb_space_blueprint.rest;

import com.atlassian.confluence.it.RestHelper;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.plugins.search.api.model.SearchResult;
import com.atlassian.confluence.plugins.search.api.model.SearchResultList;
import com.sun.jersey.api.client.WebResource;

import java.util.List;
import java.util.Set;

/**
 * Searches for content with the {@link SearchResource}.
 */
public class SearchRestHelper
{
    private final String baseUrl;

    public SearchRestHelper(String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    public List<SearchResult> search(Space space, String queryString, Set<String> labels, User user)
    {
        String url = baseUrl + "/rest/knowledge-base/1.0/search";

        WebResource resource = RestHelper.newResource(url, user)
            .queryParam("spaceKey", space.getKey())
            .queryParam("queryString", queryString)
            .queryParam("user", user.getUsername());

        if (labels != null)
        {
            for (String label : labels)
            {
                resource = resource.queryParam("labels", label);
            }
        }

        SearchResultList resultList = RestHelper.doGet(resource, SearchResultList.class, "application/json");
        return resultList.getResults();
    }
}
