package it.rest.com.atlassian.confluence.plugins.confluence_kb_space_blueprint;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rest.AbstractRestAcceptanceTest;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.KbPluginHelper;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.rest.SearchResource;
import com.atlassian.confluence.plugins.confluence_kb_space_blueprint.rest.SearchRestHelper;
import com.atlassian.confluence.plugins.search.api.model.SearchResult;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests the {@link SearchResource} exposed for ServiceDesk.
 */
public class SearchRestAcceptanceTest extends AbstractRestAcceptanceTest
{
    private SearchRestHelper restHelper;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        new KbPluginHelper(rpc).installPluginIfNotInstalled();
        restHelper = new SearchRestHelper(baseUrl);
    }

    // CONFDEV-18724
    public void testSearchResultsIncludeLikeCount()
    {
        // Create 3 pages to be returned by the search.
        int index = 1;
        Page page1 = createPrinterJamPage(index++);
        Page page2 = createPrinterJamPage(index++);
        Page page3 = createPrinterJamPage(index);

        // Add some Likes
        rpc.likes.like(page1, User.ADMIN);
        rpc.likes.like(page1, User.TEST);
        rpc.likes.like(page2, User.ADMIN);

        // Prepare to search!
        rpc.flushIndexQueue();

        List<SearchResult> results = restHelper.search(Space.TEST, "Jam", null, User.ADMIN);
        assertResultsMatchPages(results, page1, page2, page3);

        assertThat(results.get(0).getMetadata().get("likes"), is("2"));
        assertThat(results.get(1).getMetadata().get("likes"), is("1"));
    }

    // CONFDEV-18724
    public void testSearchResultsAreFilteredByLabel()
    {
        // Create 3 pages to be returned by the search.
        int index = 1;
        Page page1 = createPrinterJamPage(index++);
        Page page2 = createPrinterJamPage(index++);
        createPrinterJamPage(index);

        // Add some labels
        rpc.addLabel("printer", page1);
        rpc.addLabel("hardware", page2);

        // Prepare to search!
        rpc.flushIndexQueue();

        // Label searching is OR not AND.
        Set<String> labels = Sets.newHashSet("printer", "hardware");
        List<SearchResult> results = restHelper.search(Space.TEST, "Jam", labels, User.ADMIN);

        assertResultsMatchPages(results, page1, page2);
    }

    private Page createPrinterJamPage(int index)
    {
        Page page = new Page(Space.TEST, "Printer Jam " + index, "Your printer is jammed.");
        rpc.createPage(page);
        return page;
    }

    private void assertResultsMatchPages(List<SearchResult> results, Page... pages)
    {
        assertThat(results.size(), is(pages.length));
        for (int i = 0; i < pages.length; i++)
        {
            assertResultMatchesPage(results.get(i), pages[i]);
        }
    }

    /**
     * This method isolates the way in which a SearchResult aligns with a Page, so that if the SearchResult format
     * changes only this one method will need updating. Which might happen...
     */
    private void assertResultMatchesPage(SearchResult result, Page page)
    {
        assertThat(result.getUrl(), containsString(page.getUrl())); // contains, because ?src=search may be appended
    }
}
