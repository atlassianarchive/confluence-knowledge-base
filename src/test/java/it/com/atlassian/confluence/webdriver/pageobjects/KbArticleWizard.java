package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Collection;

public class KbArticleWizard extends ConfluenceAbstractPageComponent
{
    @ElementBy(id = "kb-article-title")
    private PageElement titleField;

    @ElementBy(id = "kb-article-labels")
    private PageElement labelsField;

    @ElementBy(xpath = "(//*[contains(concat(' ', @class, ' '), 'create-dialog-create-button')])[last()]")
    private PageElement submit;

    public KbArticleWizard setTitle(String title)
    {
        titleField.clear();
        titleField.type(title);
        return this;
    }

    public KbArticleWizard addLabels(Collection<String> labels)
    {
        LabelPicker labelPicker = getLabelPicker();

        for (String label : labels)
        {
            labelPicker.inputLabel(label)
                       .selectLabel(label);
        }

        return this;
    }

    public KbArticleWizard addLabel(String label)
    {
        labelsField.type(label);
        labelsField.type(", ");
        return this;
    }

    public KbArticleWizard inputLabel(String label)
    {
        getLabelPicker().inputLabel(label);
        return this;
    }

    public <T> T submit(Class<T> clazz)
    {
        submit.click();
        return pageBinder.bind(clazz);
    }

    public String getTitleError()
    {
        WebElement errorDiv = driver.findElement(By.cssSelector("#kb-article-title + .error"));
        return errorDiv.getText();
    }

    public LabelPicker getLabelPicker()
    {
        return pageBinder.bind(LabelPicker.class, By.id("kb-article-wizard-page-form"));
    }
}

