package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.google.common.collect.Lists;
import it.com.atlassian.confluence.plugins.createcontent.BlueprintWebDriverTestHelper;
import it.com.atlassian.confluence.webdriver.pageobjects.KbArticleWizard;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TroubleshootingBlueprintTest extends AbstractWebDriverTest
{
    static final String CREATE_DIALOG_MODULE_KEY = KnowledgeBaseSpaceBlueprintTest.PLUGIN_KEY + ":kb-troubleshooting-item";
    static final String INDEX_PAGE_TITLE = "Troubleshooting articles";

    private final String testPageTitle = "Troubleshooting Article Test";
    private BlueprintWebDriverTestHelper helper;

    @Before
    public void setUp() throws Exception
    {
        helper = new BlueprintWebDriverTestHelper(rpc, product, User.ADMIN, CREATE_DIALOG_MODULE_KEY);
    }

    @Test
    public void createTroubleshootingPage() throws Exception
    {
        KbArticleWizard wizard = helper.loginAndChooseBlueprint(KbArticleWizard.class);
        wizard.setTitle(testPageTitle);
        CreatePage editor = wizard.submit(CreatePage.class);

        helper.saveEditorAndCheckIndexPageContent(editor, Space.TEST, INDEX_PAGE_TITLE);
    }

    @Test
    public void noLabelsSpecified() throws Exception
    {
        String title = "Troubleshooting Article Test";

        KbArticleWizard wizard = helper.loginAndChooseBlueprint(KbArticleWizard.class);
        CreatePage createPage = wizard.setTitle(title)
                                      .submit(CreatePage.class);
        assertEquals(title, createPage.getTitle());
        ViewPage viewPage = createPage.save();

        assertFalse(viewPage.getTextContent().contains("Error rendering macro"));

        List<String> labels = viewPage.getLabels();
        assertEquals(1, labels.size());
        assertTrue(labels.contains("kb-troubleshooting-article"));
    }

    @Test
    public void wizardValidatesTitle() throws Exception
    {
        rpc.createPage(new Page(Space.TEST, testPageTitle, ""));

        KbArticleWizard wizard = helper.loginAndChooseBlueprint(KbArticleWizard.class);
        wizard = wizard.submit(KbArticleWizard.class);
        assertEquals("Title is required.", wizard.getTitleError());

        wizard = wizard.setTitle(testPageTitle).submit(KbArticleWizard.class);
        assertEquals("A page with this name already exists.", wizard.getTitleError());
    }

    @Test
    public void onlyRecentlyUsedLabelsForSpaceShown() throws Exception
    {
        Page title1 = rpc.getExistingPage(rpc.createPage(new Page(Space.TEST, "title1", "")));
        Space newSpace = rpc.createSpace(new Space("NEW", "My new awesome space"));
        Page adminPage = rpc.getExistingPage(rpc.createPage(new Page(newSpace, "admin's page", "")));

        rpc.addLabel("testing", title1);
        rpc.addLabel("tesselating", adminPage);

        Set<String> labelResults  = helper.loginAndChooseBlueprint(Space.TEST, KbArticleWizard.class)
                                                 .getLabelPicker()
                                                 .inputLabel("tes")
                                                 .getLabelResults();


        assertTrue(labelResults.contains("testing"));
        assertFalse(labelResults.contains("tesselating"));
    }

    @Test
    public void labelsAddedToPage() throws Exception
    {
        List<String> labels = Lists.newArrayList("test", "cat", "pig");

        ViewPage page = helper.loginAndChooseBlueprint(KbArticleWizard.class)
                .addLabels(labels)
                .setTitle(testPageTitle)
                .submit(CreatePage.class)
                .save();

        assertTrue(page.getLabels().containsAll(labels));
    }

    @Test
    public void relatedContentAddedToPage() throws Exception
    {
        String pigTitle = "Pig page";
        String catTitle = "Cat page";

        List<String> labels = Lists.newArrayList("test", "cat", "pig");

        Page pigPage = rpc.getExistingPage(rpc.createPage(new Page(Space.TEST, pigTitle, "")));
        rpc.addLabel("pig", pigPage);
        Page catPage = rpc.getExistingPage(rpc.createPage(new Page(Space.TEST, catTitle, "")));
        rpc.addLabel("cat", catPage);
        rpc.flushIndexQueue();

        ViewPage page = helper.loginAndChooseBlueprint(KbArticleWizard.class)
                .addLabels(labels)
                .setTitle(testPageTitle)
                .submit(CreatePage.class)
                .save();

        assertTrue(page.getTextContent().contains(pigTitle));
        assertTrue(page.getTextContent().contains(catTitle));
    }


}
