package it.com.atlassian.confluence.webdriver.pageobjects;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.openqa.selenium.By;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.PageElement;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LabelPicker extends ConfluenceAbstractPageComponent
{

    private By scope;
    private String dropdownItemsClass = "select2-result-label";

    public LabelPicker(By scope)
    {
        this.scope = scope;
    }

    public LabelPicker inputLabel(String labelName)
    {
        getLabelInput().type(labelName);
        return this;
    }

    public LabelPicker selectLabel(String labelName)
    {
        waitUntilTrue(getLabelResultsField().timed().isVisible());
        PageElement labelResult = getLabelResult(labelName);
        labelResult.click();
        return this;
    }

    /**
     *
     * @param labelName the label to look for
     * @return the PageElement representing the label in the labels dropdown, or null if not found.
     */
    private PageElement getLabelResult(String labelName)
    {
        String newLabel = "\"" + labelName + "\" - add a new label";

        // Need to wait until the dropdown is populated with at least one select2 option.
        waitUntilTrue(getLabelResultsField().find(By.className(dropdownItemsClass)).timed().isVisible());

        List<PageElement> all = getLabelResultsField().findAll(By.className(dropdownItemsClass));
        for (PageElement pageElement : all)
        {
            String text = pageElement.getText();
            if (labelName.equals(text) || newLabel.equals(text))
            {
                return pageElement;
            }
        }

        return null;
    }

    public PageElement getLabelInput()
    {
        return pageElementFinder.find(scope).find(By.cssSelector(".labels-autocomplete .select2-input"));
    }

    public PageElement getLabelResultsField()
    {
        return pageElementFinder.find(By.className("labels-dropdown"));
    }

    public Set<String> getLabelResults()
    {
        // Need to wait until the dropdown is populated with at least one select2 option.
        waitUntilTrue(getLabelResultsField().find(By.className(dropdownItemsClass)).timed().isVisible());

        Collection<String> results = Collections2.transform(getLabelResultsField().findAll(By.className(dropdownItemsClass)), new Function<PageElement, String>()
        {
            @Override
            public String apply(@Nullable PageElement pageElement) {
                return pageElement.getText();
            }
        });
        return new HashSet<String>(results);
    }
}
