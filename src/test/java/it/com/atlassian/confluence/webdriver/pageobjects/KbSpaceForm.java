package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.pageobjects.elements.*;
import it.com.atlassian.confluence.plugins.createcontent.pageobjects.component.form.*;

public class KbSpaceForm extends AddSpaceForm
{
    @ElementBy(id="kb-space-desc")
    private PageElement description;

    public KbSpaceForm typeDescription(String description)
    {
        this.description.clear();
        this.description.type(description);
        return this;
    }
}