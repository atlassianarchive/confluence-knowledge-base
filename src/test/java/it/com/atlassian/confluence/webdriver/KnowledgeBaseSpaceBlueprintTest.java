package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.it.GlobalPermission;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.util.test.annotations.ExportedTestClass;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.atlassian.confluence.plugins.createcontent.BlueprintWebDriverTestHelper;
import it.com.atlassian.confluence.plugins.createcontent.SpaceBlueprintWebDriverTestHelper;
import it.com.atlassian.confluence.plugins.createcontent.pageobjects.BlueprintDialog;
import it.com.atlassian.confluence.plugins.createcontent.pageobjects.DashboardPage;
import it.com.atlassian.confluence.plugins.createcontent.pageobjects.component.dialog.CreateSpaceDialog;
import it.com.atlassian.confluence.webdriver.pageobjects.KbArticleWizard;
import it.com.atlassian.confluence.webdriver.pageobjects.KbSpaceForm;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@ExportedTestClass
public class KnowledgeBaseSpaceBlueprintTest extends AbstractWebDriverTest
{
    static final String PLUGIN_KEY = "com.atlassian.confluence.plugins.confluence-knowledge-base";
    static final String CREATE_DIALOG_MODULE_KEY = PLUGIN_KEY + ":kb-blueprint-item";

    private SpaceBlueprintWebDriverTestHelper spaceHelper;
    private BlueprintWebDriverTestHelper howToHelper;

    @Before
    public void setUp() throws Exception
    {
        rpc.logIn(User.ADMIN);
        rpc.grantPermission(GlobalPermission.CREATE_SPACE, User.TEST);

        spaceHelper = new SpaceBlueprintWebDriverTestHelper(rpc, product, User.TEST, CREATE_DIALOG_MODULE_KEY);
        howToHelper = new BlueprintWebDriverTestHelper(rpc, product, User.TEST, HowToBlueprintTest.CREATE_DIALOG_MODULE_KEY);
    }

    @Test
    public void testHomePage() throws Exception
    {
        String spaceName = "My Knowledge Base";
        String description = "My awesome knowledge base space";

        DashboardPage dashboardPage = product.login(User.TEST, DashboardPage.class);
        ViewPage viewHomepage = createKbSpace(dashboardPage.openCreateSpaceDialog(), "KB", spaceName, description);

        assertEquals(spaceName, viewHomepage.getTitle());
        assertTrue(viewHomepage.getTextContent().contains("Welcome!"));
        assertTrue(viewHomepage.getTextContent().contains(description));
    }

    @Test
    public void testArticlesArePromoted() throws Exception
    {
        DashboardPage dashboardPage = product.login(User.TEST, DashboardPage.class);
        createKbSpace(dashboardPage.openCreateSpaceDialog(), "KB", "My Knowledge Base", "");

        BlueprintDialog blueprintDialog = spaceHelper.openCreateDialog();
        assertTrue(blueprintDialog.hasTemplateItem(HowToBlueprintTest.CREATE_DIALOG_MODULE_KEY));
        assertTrue(blueprintDialog.hasTemplateItem(TroubleshootingBlueprintTest.CREATE_DIALOG_MODULE_KEY));
        assertTrue(blueprintDialog.isShowMoreLinkPresent());
    }

    @Test
    public void relatedArticlesInCurrentSpaceOnly()
    {
        rpc.createSpace(Space.TEST2);
        Page anotherHowTo = new Page(Space.TEST2, "How to page in test space 2", "foo bar");
        rpc.createPage(anotherHowTo);
        rpc.addLabel(HowToBlueprintTest.HOW_TO_LABEL, anotherHowTo);

        Space kbSpace = new Space("KB", "Confluence KB");
        DashboardPage dashboardPage = product.login(User.TEST, DashboardPage.class);
        ViewPage homepage = createKbSpace(dashboardPage.openCreateSpaceDialog(), kbSpace.getKey(), kbSpace.getName(), "");

        assertFalse(homepage.getMainContent().find(By.linkText(anotherHowTo.getTitle())).isPresent());

        KbArticleWizard wizard = howToHelper.openCreateDialogAndChooseBlueprint(KbArticleWizard.class);
        CreatePage createPage = wizard.setTitle("How to page in space KB").submit(CreatePage.class);
        ViewPage viewPage = createPage.save();
        assertFalse(viewPage.getMainContent().find(By.linkText(anotherHowTo.getTitle())).isPresent());
    }

    private ViewPage createKbSpace(CreateSpaceDialog spaceDialog, String key, String name, String description)
    {
        spaceDialog.selectSpaceBlueprint(CREATE_DIALOG_MODULE_KEY);
        spaceDialog.next();
        KbSpaceForm form = product.getPageBinder().bind(KbSpaceForm.class);
        form.setName(name);
        form.setKey(key);
        if (description.length() > 0)
            form.typeDescription(description);

        return spaceDialog.submit(ViewPage.class);
    }
}
